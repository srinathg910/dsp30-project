# Imarticus DSP30 Project
Website: https://srinathg910.gitlab.io/dsp30-project/

---------

**Notes** for using/testing this project:
- Folder `pickle` locally stores a cache of the dataset.
- Folder `public` contains the websites HTML (CSS, JS) files.
- Folder `question` contains the problem description.
- Folder `data` contains raw data used.
- Folder `src` contains the source code
    - `src/api` contains the common data fetch and evaludate related code
    - `src/utils` contains helpers to the project
    - You can use `requirements.txt` to install apps
    - You can run `jupyter notebook` in root of the project to start the notebook server

-----------

**Sitemap:**
- **Landing page**: https://srinathg910.gitlab.io/dsp30-project/
- **EDA**: https://srinathg910.gitlab.io/dsp30-project/eda_summary.html
- **EDA Visualization**: https://srinathg910.gitlab.io/dsp30-project/models/output/EDA.html
- Models: https://srinathg910.gitlab.io/dsp30-project/models/summary.html
- **Recommendations**: https://srinathg910.gitlab.io/dsp30-project/recommendations.html

-----------

**Wishlist:**
- I would like to use `desc` field to better understand the problem and evaluate its re-paying likelihood
    - This would be in the domain of Natural Language which is a big undertaking on its own.
    - User written notes are NOT always grammatically correct and/or well formed which will need to be addressed.