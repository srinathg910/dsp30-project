$(document).ready(function () {
    $('#summary').DataTable({
        "dom": "t",
        "paging": false,
        "order": [[1, "desc"]],
        "ajax": "summaryData.txt",
        "columns": [
            {
                "data": "model",
                render: function(data, type, row) {
                    return `<a class="bold" href="output/${row.file}" target="_blank"><i class="fas fa-link"></i>&nbsp;&nbsp;${data}</a>`;
                }
            },
            {"data": "recall"},
            {"data": "specificity"},
            {"data": "precision"},
            {"data": "accuracy"},
            {"data": "auc"},
            {"data": "f1"},
            {"data": "threshold"},
            {"data": "features"},
        ],
        "columnDefs": [
            {"targets": 0},
            {"targets": 1, className: "dt-body-center bold"},
            {"targets": 2, className: "dt-body-center"},
            {"targets": 3, className: "dt-body-center"},
            {"targets": 4, className: "dt-body-center"},
            {"targets": 5, className: "dt-body-center"},
            {"targets": 6, className: "dt-body-center"},
            {"targets": 7, className: "dt-body-center"},
            {"targets": 8, className: "dt-body-center"},
        ]
    });
});