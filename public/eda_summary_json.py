import json


all = ['loan_amnt', 'funded_amnt', 'funded_amnt_inv', 'term', 'int_rate', 'installment',
       'grade', 'sub_grade', 'emp_length', 'home_ownership', 'annual_inc',
       'verification_status', 'issue_d', 'pymnt_plan', 'purpose', 'dti', 'delinq_2yrs',
       'inq_last_6mths', 'open_acc', 'pub_rec', 'revol_bal', 'revol_util', 'total_acc',
       'initial_list_status', 'out_prncp', 'out_prncp_inv', 'total_pymnt',
       'total_pymnt_inv', 'total_rec_prncp', 'total_rec_int', 'total_rec_late_fee',
       'recoveries', 'collection_recovery_fee', 'last_pymnt_amnt', 'next_pymnt_d',
       'collections_12_mths_ex_med', 'mths_since_last_major_derog', 'application_type',
       'acc_now_delinq', 'tot_coll_amt', 'tot_cur_bal', 'total_rev_hi_lim', ]

annova_ok = ['id', 'member_id', 'term', 'int_rate', 'grade', 'sub_grade',
             'home_ownership', 'annual_inc', 'verification_status', 'desc', 'purpose',
             'title', 'inq_last_6mths', 'revol_util', 'initial_list_status',
             'out_prncp', 'out_prncp_inv', 'total_pymnt', 'total_pymnt_inv',
             'total_rec_prncp', 'total_rec_int', 'total_rec_late_fee', 'recoveries',
             'collection_recovery_fee', 'last_pymnt_d', 'last_pymnt_amnt',
             'next_pymnt_d', 'mths_since_last_major_derog', 'tot_cur_bal',
             'total_rev_hi_lim']

chisq_ok = ['id', 'member_id', 'loan_amnt', 'funded_amnt', 'funded_amnt_inv',
            'sub_grade', 'emp_title', 'annual_inc', 'desc', 'title', 'revol_bal',
            'out_prncp', 'out_prncp_inv', 'total_pymnt', 'total_pymnt_inv',
            'total_rec_prncp', 'total_rec_int', 'total_rec_late_fee', 'recoveries',
            'collection_recovery_fee', 'last_pymnt_d', 'last_pymnt_amnt',
            'next_pymnt_d', 'annual_inc_joint', 'tot_coll_amt', 'tot_cur_bal',
            'total_bal_il', 'il_util', 'max_bal_bc', 'total_rev_hi_lim']

rfe_1_ok = ['funded_amnt', 'funded_amnt_inv', 'int_rate', 'installment', 'sub_grade',
            'emp_length', 'dti', 'open_acc', 'revol_util', 'total_acc', 'out_prncp',
            'out_prncp_inv', 'total_pymnt_inv', 'total_rec_prncp', 'total_rec_int',
            'recoveries', 'collection_recovery_fee', 'last_pymnt_amnt',
            'mths_since_last_major_derog', 'tot_coll_amt']

rfe_2_ok = ['loan_amnt', 'funded_amnt', 'funded_amnt_inv', 'term', 'int_rate',
            'installment', 'grade', 'sub_grade', 'dti', 'initial_list_status',
            'out_prncp', 'out_prncp_inv', 'total_pymnt', 'total_pymnt_inv',
            'total_rec_prncp', 'total_rec_int', 'recoveries', 'collection_recovery_fee',
            'last_pymnt_amnt', 'next_pymnt_d']

select_from_model_1_ok = ['id', 'member_id', 'loan_amnt', 'funded_amnt',
                          'funded_amnt_inv', 'term', 'installment', 'out_prncp',
                          'out_prncp_inv', 'total_pymnt', 'total_pymnt_inv',
                          'total_rec_prncp', 'total_rec_int', 'recoveries',
                          'collection_recovery_fee', 'last_pymnt_d', 'last_pymnt_amnt',
                          'next_pymnt_d']


def get_row(var):
    count = 0

    is_annova = True if var in annova_ok else False
    if is_annova:
        count = count + 1

    is_chisq = True if var in chisq_ok else False
    if is_chisq:
        count = count + 1

    is_rfe1 = True if var in rfe_1_ok else False
    if is_rfe1:
        count = count + 1

    is_rfe2 = True if var in rfe_2_ok else False
    if is_rfe2:
        count = count + 1

    is_select_from_model_1_ok = True if var in select_from_model_1_ok else False
    if is_select_from_model_1_ok:
        count = count + 1

    return {
        "feature": var,
        "annova": is_annova,
        "chisq": is_chisq,
        "rfe1": is_rfe1,
        "rfe2": is_rfe2,
        "select_from_model_1": is_select_from_model_1_ok,
        "score": count
    }


all_dict = [get_row(x) for x in all]

json.dumps({"data": all_dict})

with open("public/eda_summary.json", 'w') as f:
    f.write(json.dumps({"data": all_dict}))
