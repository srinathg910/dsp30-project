getCellMarker = function(data) {
    console.log(data);
    if (data) {
        return "<span class='hide'>1</span><i class='fa fa-check style-ok-nok style-ok' aria-hidden='true'></i>";
    }
    else {
        return "<span class='hide'>2</span><i class='fa fa-times style-ok-nok style-nok' aria-hidden='true'></i>";
    }
}

$(document).ready(function () {
    var featureTable = $('#feature-selection').DataTable({
        "dom": "ft",
        "paging": false,
        "scrollY": '85vh',
        "scrollCollapse": true,
        "order": [[6, "desc"]],
        "ajax": "eda_summary.json",
        "columns": [
            {"data": "feature"},
            {
                "data": "annova",
                render: function(data, type, row) {
                    return getCellMarker(data);
                }
            },
            {
                "data": "chisq",
                render: function(data, type, row) {
                    return getCellMarker(data);
                }
            },
            {
                "data": "rfe1",
                render: function(data, type, row) {
                    return getCellMarker(data);
                }
            },
            {
                "data": "rfe2",
                render: function(data, type, row) {
                    return getCellMarker(data);
                }
            },
            {
                "data": "select_from_model_1",
                render: function(data, type, row) {
                    return getCellMarker(data);
                }
            },
            {"data": "score"}
        ],
        "columnDefs": [
            {"targets": 0, className: "bold"},
            {"targets": 1, className: "dt-body-center"},
            {"targets": 2, className: "dt-body-center"},
            {"targets": 3, className: "dt-body-center"},
            {"targets": 4, className: "dt-body-center"},
            {"targets": 5, className: "dt-body-center"},
            {"targets": 6, className: "dt-body-center"},
        ]
    });
});