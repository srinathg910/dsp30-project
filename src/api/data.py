import pandas
import os
import numpy

from src.api.data_conf import metadata, datatypes


SELECTED_VAR = ["issue_d", "default_ind", "int_rate", "out_prncp",
                           "out_prncp_inv", "total_rec_late_fee", "recoveries",
                           "collection_recovery_fee", "last_pymnt_amnt",
                           "tot_cur_bal", "total_rec_prncp", "total_rev_hi_lim"]

SELECTED_VAR2 = ["issue_d", "default_ind", "int_rate", "out_prncp", "out_prncp_inv",
                 "recoveries", "collection_recovery_fee", "last_pymnt_amnt",
                 "tot_cur_bal", "total_rec_prncp", "total_pymnt", "total_pymnt_inv",
                 "revol_bal", "revol_util"]

# RecursiveFeatureElimination_LogisticRegression
SELECTED_VAR3 = ['issue_d', 'default_ind', 'loan_amnt', 'funded_amnt',
                 'funded_amnt_inv', 'int_rate', 'installment', 'dti', 'open_acc',
                 'revol_util', 'total_acc', 'out_prncp', 'out_prncp_inv', 'total_pymnt',
                 'total_pymnt_inv', 'total_rec_prncp', 'total_rec_int', 'recoveries',
                 'collection_recovery_fee', 'last_pymnt_amnt',
                 'mths_since_last_major_derog', 'tot_coll_amt']

SELECTED_VAR4 = ['issue_d', 'default_ind', 'loan_amnt', 'funded_amnt', 'funded_amnt_inv', 'term', 'int_rate',
                 'installment', 'grade', 'sub_grade', 'dti', 'initial_list_status',
                 'out_prncp', 'out_prncp_inv', 'total_pymnt', 'total_pymnt_inv',
                 'total_rec_prncp', 'total_rec_int', 'recoveries',
                 'collection_recovery_fee', 'last_pymnt_amnt', 'next_pymnt_d']

# SelectFromModel_ExtraTreesClassifier
SELECTED_VAR5 = ['issue_d', 'default_ind', 'loan_amnt', 'funded_amnt',
                 'funded_amnt_inv', 'term', 'installment', 'out_prncp', 'out_prncp_inv',
                 'total_pymnt', 'total_pymnt_inv', 'total_rec_prncp', 'total_rec_int',
                 'recoveries', 'collection_recovery_fee', 'last_pymnt_d',
                 'last_pymnt_amnt', 'next_pymnt_d']

# Feature Selection eda_summary (Common ones)
SELECTED_VAR6 = ['issue_d', 'default_ind', "out_prncp", "out_prncp_inv", "total_pymnt_inv", "total_rec_prncp",
                 "total_rec_int", "recoveries", "collection_recovery_fee", "last_pymnt_amnt"]

def print_data_sizes():
    print(f"1: {len(SELECTED_VAR)}")
    print(f"2: {len(SELECTED_VAR2)}")
    print(f"3: {len(SELECTED_VAR3)}")
    print(f"4: {len(SELECTED_VAR4)}")
    print(f"5: {len(SELECTED_VAR5)}")
    print(f"6: {len(SELECTED_VAR6)}")


class DataStore:
    def __init__(self, var_set_type):
        self.file = "data/XYZCorp_LendingData.txt"
        self.var_set_type = var_set_type

        self.raw_pickle = f"pickle/raw{var_set_type}.pickle"
        self.train_pickle = f"pickle/train{var_set_type}.pickle"
        self.test_pickle = f"pickle/test{var_set_type}.pickle"

        # Data Frame
        self.df_raw = None  # Raw from file
        self.df = None  # After processing
        self.meta_dict = None
        self.fetch_raw_data()
        self.pre_processing()

        self.train = None
        self.test = None

        self.set_variable_metadata()
        self.set_train_data()
        self.set_test_data()

    def set_variable_metadata(self):
        self.meta_dict = dict()
        for k, v in datatypes.items():
            self.meta_dict[k] = (v, metadata.get(k), )

    def pre_processing(self):
        if self.var_set_type == 1:
            self.df = self.df[SELECTED_VAR]
        elif self.var_set_type == 2:
            self.df = self.df[SELECTED_VAR2]
        elif self.var_set_type == 3:
            self.df = self.df[SELECTED_VAR3]
        elif self.var_set_type == 4:
            self.df = self.df[SELECTED_VAR4]
        elif self.var_set_type == 5:
            self.df = self.df[SELECTED_VAR5]
        elif self.var_set_type == 6:
            self.df = self.df[SELECTED_VAR6]

        # Custom handling for issue_d
        # Convert Date Format
        self.df.issue_d = pandas.to_datetime(self.df.issue_d)

        # Handle Category NA
        for cat_var in self.df.select_dtypes(include=[numpy.object]).columns.values:
            self.df[cat_var] = self.df[cat_var].fillna("NA")
        self.df = pandas.get_dummies(self.df, columns=self.df.select_dtypes(include=[numpy.object]).columns.values)

        # Handle Numeric NA
        for num_var in self.df.select_dtypes(include=[numpy.float64]).columns.values:
            self.df[num_var] = self.df[num_var].fillna(0)

    def fetch_raw_data(self):
        self.df_raw = pandas.read_csv(self.file, sep="\t", dtype=datatypes)
        self.df = self.df_raw.copy()

    def set_train_data(self):
        # June 2007 - May 2015
        self.train = self.df[(self.df["issue_d"] > "2007-06-01") & (self.df["issue_d"] < "2015-06-01")]

    def set_test_data(self):
        # June 2015 - Dec 2015
        self.test = self.df[(self.df["issue_d"] >= "2015-06-01") & (self.df["issue_d"] < "2016-01-01")]


class LoadDataStore:
    """
    Parts of the data to be picked
    1. Training Data
    2. Testing Data
    """
    def __init__(self, var_set_type):
        self.train = None
        self.test = None
        self.raw = None

        # Whether to use cache for faster processing
        self.use_cache = True
        self.var_set_type = var_set_type

    def force_load(self):
        ds = DataStore(self.var_set_type)
        self.raw = ds.df_raw
        self.train = ds.train
        self.test = ds.test

    def load_raw(self):
        ds = DataStore(self.var_set_type)

        raw_pickle_exists = os.path.exists(ds.raw_pickle)
        if raw_pickle_exists and self.use_cache:
            self.raw = pandas.read_pickle(ds.raw_pickle)
        elif self.raw:
            # train was already loaded from source
            self.raw.to_pickle(ds.raw_pickle)
        else:
            self.force_load()
            self.raw.to_pickle(ds.raw_pickle)

        return self.raw

    def load_train(self):
        ds = DataStore(self.var_set_type)

        train_pickle_exists = os.path.exists(ds.train_pickle)
        if train_pickle_exists and self.use_cache:
            self.train = pandas.read_pickle(ds.train_pickle)
        elif self.train:
            # train was already loaded from source
            self.train.to_pickle(ds.train_pickle)
        else:
            self.force_load()
            self.train.to_pickle(ds.train_pickle)

        return self.train

    def load_test(self):
        ds = DataStore(self.var_set_type)

        test_pickle_exists = os.path.exists(ds.test_pickle)
        if test_pickle_exists and self.use_cache:
            self.test = pandas.read_pickle(ds.test_pickle)
        elif self.test:
            # train was already loaded from source
            self.test.to_pickle(ds.test_pickle)
        else:
            self.force_load()
            self.test.to_pickle(ds.test_pickle)

        return self.test

    @staticmethod
    def get_raw(var_set_type):
        return LoadDataStore(var_set_type).load_raw()

    @staticmethod
    def get_train(var_set_type):
        return LoadDataStore(var_set_type).load_train()

    @staticmethod
    def get_test(var_set_type):
        return LoadDataStore(var_set_type).load_test()


def get_train_test(var_set_type):
    train = LoadDataStore.get_train(var_set_type)
    x_train = train.drop(["default_ind", "issue_d"], axis=1)
    y_train = train[["default_ind"]].values.ravel()

    test = LoadDataStore.get_test(var_set_type)
    x_test = test.drop(["default_ind", "issue_d"], axis=1)
    y_test = test[["default_ind"]].values.ravel()

    return x_train, y_train, x_test, y_test
