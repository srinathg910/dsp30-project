import pickle


def save_model(name, model):
    pickle.dump(model, open(f"pickle/{name}.pickle", "wb"))


def get_model(name, model):
    return pickle.load(open(f"pickle/{name}.pickle", "rb"))
