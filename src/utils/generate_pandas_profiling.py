import pandas

from pandas_profiling import ProfileReport
from src.api.data import LoadDataStore


if __name__ == "__main__":
    profile = ProfileReport(LoadDataStore.get_raw(var_set_type=1), title='Report', minimal=True)
    profile.to_file("public/auto_eda/report.html")

    profile = ProfileReport(
            pandas.concat([LoadDataStore.get_train(var_set_type=1), LoadDataStore.get_test(var_set_type=1)]),
            title='Report')
    profile.to_file("public/auto_eda/mod.html")
