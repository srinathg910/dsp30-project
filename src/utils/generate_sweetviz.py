import sweetviz

from src.api.data import LoadDataStore


report = sweetviz.analyze(LoadDataStore.get_raw(var_set_type=1))
report.show_html(filepath="public/auto_eda/SWEETVIZ_REPORT.html")
