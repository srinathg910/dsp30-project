import numpy

from sklearn.metrics import roc_curve, roc_auc_score, precision_recall_curve, confusion_matrix
from matplotlib import pyplot

from src.utils.confusion_matrix import make_confusion_matrix


class ModelVizHelper:
    def __init__(self, model, x_test, y_test):
        self.model = model
        self.x_test = x_test
        self.y_test = y_test

        # Probability of 1
        self.prob_for_1 = model.predict_proba(self.x_test)[:, 1]

    def plot_roc_curve(self):
        no_model_fpr, no_model_tpr, thresholds = roc_curve(self.y_test, [0 for _ in range(len(self.y_test))])
        model_fpr, model_tpr, thresholds = roc_curve(self.y_test, self.prob_for_1)

        pyplot.figure(figsize=(6, 6))
        pyplot.plot(no_model_fpr, no_model_tpr, linestyle='--')
        area_under_curve = round(roc_auc_score(self.y_test, self.model.predict(self.x_test)), 2)
        pyplot.title(f"Area Under Curve: {area_under_curve}")
        pyplot.plot(model_fpr, model_tpr, marker='.', label="Model")
        pyplot.xlabel('False Positive Rate')
        pyplot.ylabel('True Positive Rate')
        pyplot.legend()
        pyplot.show()

    def plot_percision_recall_curve(self):
        precision, recall, thresholds = precision_recall_curve(self.y_test, self.prob_for_1)

        pyplot.title("Precision-Recall vs Threshold Chart")
        pyplot.plot(thresholds, precision[: -1], "b--", label="Precision")
        pyplot.plot(thresholds, recall[: -1], "r--", label="Recall")
        pyplot.ylabel("Precision, Recall")
        pyplot.xlabel("Threshold")
        pyplot.legend(loc="lower left")
        pyplot.ylim([0, 1])

    def plot_confusion_matrix(self, threshold=0.5, draw=False):
        labels = ['True Neg', 'False Pos', 'False Neg', 'True Pos']
        categories = ['Not Defaulted', 'Defaulted']

        y_pred = [1 if x > threshold else 0 for x in self.prob_for_1]
        conf_matrix = confusion_matrix(self.y_test, y_pred)

        if draw:
            make_confusion_matrix(conf_matrix, group_names=labels, categories=categories, cmap='binary')
        else:
            # intended print() when run at Jupyter
            return conf_matrix
